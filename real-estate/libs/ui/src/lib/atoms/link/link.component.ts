import { Component } from '@angular/core';

@Component({
  selector: 'real-estate-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss'],
})
export class LinkComponent {}
