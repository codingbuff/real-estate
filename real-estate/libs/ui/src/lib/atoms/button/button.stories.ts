// Button.stories.ts

import { Meta, Story } from '@storybook/angular';

import { ButtonComponent } from './button.component';

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/angular/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Button',
  component: ButtonComponent,
} as Meta;

//👇 We create a “template” of how args map to rendering
const Template: Story = (args) => ({
    props: args,
  });
  
  // 👇 Each story then reuses that template
  export const Primary= Template.bind({});
  Primary.args = {
    label: 'Primary',
    backgroundColor: '#ff0',
  };
  
  
  export const Secondary= Template.bind({});
  Secondary.args = {
    args: {
      ...Primary.args,
      label: 'Secondary',
    },
  };