import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './atoms/button/button.component';
import { LinkComponent } from './atoms/link/link.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ButtonComponent, LinkComponent],
})
export class UiModule {}
